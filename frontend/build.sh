#!/usr/bin/env bash

set -eux
set -o pipefail

VERSION=$(cat ./VERSION)

case "$OSTYPE" in
    darwin*)  PLATFORM="OSX" ;;
    linux*)   PLATFORM="LINUX" ;;
    bsd*)     PLATFORM="BSD" ;;
    *)        PLATFORM="UNKNOWN" ;;
esac

replace() {
    RE=$1
    shift
    if [[ "${PLATFORM}" == "OSX" || "${PLATFORM}" == "BSD" ]]; then
        sed -i "" "$RE" $*
    elif [ "${PLATFORM}" == "LINUX" ]; then
        sed -i "$RE" $*
    fi
}

tsc
replace "s#<%= SERVER_PATH %>#${SERVER_PATH}#g" build/*.js
replace "s#<%= VERSION %>#${VERSION}#g" build/*.js
tar -czvf dist.tgz index.html assets/ build/
