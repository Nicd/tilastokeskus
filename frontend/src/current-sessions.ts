import { CLOCK_24H, EVENT_LIST_MAX_VIEWS, LOCALE, TIMEZONE } from "./config.js";
import { EventStore } from "./event-store.js";
import { IPageView } from "./page-view.js";
import { Table } from "./table.js";

export class CurrentSessions extends Table {
  private viewData: IPageView[];
  private dateFormatter: Intl.DateTimeFormat;

  constructor(el: HTMLElement, eventStore: EventStore) {
    super(el, eventStore, ["Time", "Session", "Path", "Referrer"]);

    this.viewData = [];
    this.dateFormatter = new Intl.DateTimeFormat(LOCALE, {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      timeZone: TIMEZONE,
      hour12: !CLOCK_24H,
    });
  }

  public add(view: IPageView) {
    this.viewData.push(view);
    this.render();
  }

  public expire(view: IPageView) {
    this.viewData = this.viewData.filter((v) => v !== view);
    this.render();
  }

  public render() {
    this.viewData.sort((a, b) => {
      return (b.at.getTime() - a.at.getTime());
    });
    this.viewData = this.viewData.slice(0, EVENT_LIST_MAX_VIEWS - 1);

    super.setData(this.viewData.map((v) => {
      let ref: string | HTMLAnchorElement;

      if (v.referrer != null) {
        ref = document.createElement("a");
        ref.href = v.referrer;
        ref.innerText = v.referrer;
        ref.target = "_blank";
      } else {
        ref = "";
      }

      return [
        this.dateFormatter.format(v.at),
        v.session.split("-")[0],
        v.path_noq,
        ref,
      ];
    }));

    super.render();
  }
}
