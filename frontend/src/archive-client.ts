const ARCHIVE_BASE_URL = "<%= SERVER_PATH %>";

const PAGE_VIEW_EVENT = "view";

/**
 * Get the URL path to use for updates about the given host.
 */
function hostUpdateUrl(host: string): string {
  return `${ARCHIVE_BASE_URL}/${host}`;
}

/**
 * Start listening for updates to the given host.
 */
export function connectListener(
  host: string,
  openCallback: (stream: EventSource, evt: Event) => void,
  msgCallback: (stream: EventSource, evt: MessageEvent) => void,
  errorCallback: (stream: EventSource, evt: ErrorEvent) => void,
): EventSource {
  const url = hostUpdateUrl(host);
  const source = new EventSource(url);

  source.addEventListener("open", (e: Event) => {
    openCallback(source, e);
  });

  source.addEventListener(PAGE_VIEW_EVENT, (e: Event): void => {
    msgCallback(source, e as MessageEvent);
  });

  source.addEventListener("error", (e: Event) => {
    errorCallback(source, e as ErrorEvent);
  });

  return source;
}
