/** How many seconds a page view is considered "current". */
export const CURRENT_THRESHOLD = 300 as const;

/** Maximum reconnect delay in seconds. */
export const MAX_RECONNECT_DELAY = 30 as const;

/** How often to check old events and clear non-current. */
export const OLD_CLEAR_INTERVAL = 1 as const;

/** Max amount of events to show in event list. */
export const EVENT_LIST_MAX_VIEWS = 20 as const;

/** Max amount of paths shown in top paths. */
export const PATH_LIST_MAX_LEN = 6 as const;

/** Locale to use for string/date/time formatting. */
export const LOCALE = "fi-FI" as const;

/** Timezone to use displaying datetimes. */
export const TIMEZONE = "Europe/Helsinki" as const;

/** Use 24 hour clock when displaying datetimes? */
export const CLOCK_24H = true as const;
