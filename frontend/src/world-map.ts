import { Component } from "./component.js";
import { CURRENT_THRESHOLD } from "./config.js";
import { EventStore } from "./event-store.js";
import { IPageView } from "./page-view.js";

/** How many seconds a spot lives on the map. */
const SPOT_LIFETIME = CURRENT_THRESHOLD;

/** How often in seconds to update spot opacity. */
const SPOT_UPDATE_INTERVAL = 1;

export class WorldMap extends Component {
  private spots: Map<string, [HTMLDivElement, Date]>;

  constructor(el: HTMLElement, eventStore: EventStore) {
    super(el, eventStore);

    this.spots = new Map();

    const img = document.createElement("img");
    img.alt = "World map";
    img.src = "assets/world-map.png";
    img.id = "world-map";

    el.append(img);

    setInterval(() => this.render(), SPOT_UPDATE_INTERVAL * 1000);
  }

  public add(view: IPageView) {
    if (view.loc_lat === 0.0 || view.loc_lon === 0.0) {
      return;
    } else {
      let spot: HTMLDivElement;

      if (this.spots.has(view.session)) {
        spot = this.spots.get(view.session)![0];
      } else {
        spot = document.createElement("div");
      }

      spot.className = "spot";
      spot.title = (view.loc_city ? `${view.loc_city}, ` : "") + `${view.loc_country} (${view.session})`;
      spot.style.top = `${100 - ((view.loc_lat + 90) / 180 * 100)}%`;
      spot.style.left = `${(view.loc_lon + 180) / 360 * 100}%`;
      spot.style.transform = "translate(-50%, -50%)";
      spot.style.opacity = String(this.spotLife(Date.now(), view.at.getTime()));

      this.el.append(spot);
      this.spots.set(view.session, [spot, view.at]);
    }
  }

  public expire() { /* Noop */ }

  public render() {
    const now = Date.now();
    for (const [session, [el, at]] of this.spots.entries()) {
      const life = this.spotLife(now, at.getTime());

      if (life <= 0) {
        this.el.removeChild(el);
        this.spots.delete(session);
      } else {
        el.style.opacity = String(life);
      }
    }
  }

  private spotLife(now: number, spotDate: number) {
    return 1 - ((now - spotDate) / (SPOT_LIFETIME * 1000));
  }
}
