import { connectListener } from "./archive-client.js";
import { MAX_RECONNECT_DELAY } from "./config.js";
import { CurrentSessions } from "./current-sessions.js";
import { EventStore } from "./event-store.js";
import { LiveCounter } from "./live-counter.js";
import { IRemotePageView, parseView } from "./page-view.js";
import { TopPaths } from "./top-paths.js";
import { WorldMap } from "./world-map.js";

let eventStore: EventStore;
let worldMap: WorldMap;
let currentyLiveCounter: LiveCounter;
let currentSessions: CurrentSessions;
let topPaths: TopPaths;
let eventSource: EventSource;
let host: string;

function openCallback(stream: EventSource, e: Event) {
  console.log("Opened event stream.");
}

function errorCallback(stream: EventSource, e: ErrorEvent) {
  console.error(e);

  stream.close();

  const delaySecs = Math.round(Math.random() * MAX_RECONNECT_DELAY);
  console.log(`Reconnecting in ${delaySecs} seconds…`);
  setTimeout(initConnection, delaySecs * 1000);
}

function msgCallback(stream: EventSource, e: MessageEvent) {
  const data = JSON.parse(e.data) as IRemotePageView;
  console.log("Received data", data);

  const parsedData = parseView(data);
  eventStore.add(parsedData);
}

function initConnection() {
  eventSource = connectListener(host, openCallback, msgCallback, errorCallback);
}

function main() {
  eventStore = new EventStore();
  currentyLiveCounter = new LiveCounter(document.getElementById("currently-live-counter")!, eventStore);
  worldMap = new WorldMap(document.getElementById("world-map")!, eventStore);
  currentSessions = new CurrentSessions(document.getElementById("current-sessions-data")!, eventStore);
  topPaths = new TopPaths(document.getElementById("top-paths-data")!, eventStore);

  const hostParam = (new URL(window.location.href)).searchParams.get("host");

  if (hostParam === null) {
    alert("Missing host! Give host as ?host= query argument.");
    throw new Error("Missing host.");
  }

  document.getElementById("title")!.textContent = hostParam;
  document.getElementById("version")!.textContent = "v<%= VERSION %>";

  host = hostParam;
  initConnection();

  // We need to close the event source to prevent an error being logged when refreshing the page
  window.addEventListener("beforeunload", () => eventSource.close());
}

if (document.readyState === "interactive") {
  main();
} else {
  document.addEventListener("readystatechange", () => {
    if (document.readyState === "interactive") {
      main();
    }
  });
}
