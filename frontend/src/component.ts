import { EventStore, IEventListener } from "./event-store.js";
import { IPageView } from "./page-view.js";

/** Component that controls an element, rendering output to it. */
export abstract class Component implements IEventListener {
  protected el: HTMLElement;
  protected eventStore: EventStore;

  constructor(el: HTMLElement, eventStore: EventStore) {
    this.el = el;
    this.eventStore = eventStore;
    this.eventStore.listen(this);
  }

  public abstract add(view: IPageView): void;

  public abstract expire(view: IPageView): void;

  public abstract render(): void;
}
