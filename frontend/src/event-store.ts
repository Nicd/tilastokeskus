import { CURRENT_THRESHOLD, OLD_CLEAR_INTERVAL } from "./config.js";
import { IPageView } from "./page-view.js";

export interface IEventListener {
  add(view: IPageView): void;
  expire(view: IPageView): void;
}

/**
 * The event store keeps track of events that can be displayed as "current" and clears old ones.
 *
 * Components can subscribe to events to get informed of event changes.
 */
export class EventStore {
  private events: IPageView[];
  private listeners: IEventListener[];

  constructor() {
    this.events = [];
    this.listeners = [];

    setInterval(() => this.clearOld(), OLD_CLEAR_INTERVAL * 1000);
  }

  public add(view: IPageView) {
    this.events.push(view);

    for (const listener of this.listeners) {
      listener.add(view);
    }
  }

  public listen(listener: IEventListener) {
    this.listeners.push(listener);
  }

  private clearOld() {
    const now = Date.now();
    const firstCurrent = this.events.findIndex((v) => (now - v.at.getTime()) <= CURRENT_THRESHOLD * 1000);
    const deleted = this.events.splice(0, firstCurrent !== -1 ? firstCurrent : this.events.length);

    for (const deletedEvent of deleted) {
      for (const listener of this.listeners) {
        listener.expire(deletedEvent);
      }
    }
  }
}
