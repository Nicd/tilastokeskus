export interface IBasePageView {
  addr: string;
  extra: object;
  scrubbed: boolean;
  session: string;
  path: string;
  path_noq: string;
  host: string;
  referrer: string;
  referrer_domain: string;
  ua: string;
  ua_name: string;
  ua_version: string;
  os_name: string;
  os_version: string;
  device_type: string;
  screen_w: number;
  screen_h: number;
  tz_offset: number;
  loc_city: string;
  loc_country: string;
  loc_lat: number;
  loc_lon: number;
  is_bot: boolean;
}

export interface IRemotePageView extends IBasePageView {
  at: string;
}

export interface IPageView extends IBasePageView {
  at: Date;
}

/**
 * Parse a page view coming from the backend.
 */
export function parseView(view: IRemotePageView): IPageView {
  return {
    at: new Date(view.at),
    addr: view.addr,
    extra: view.extra,
    scrubbed: view.scrubbed,
    session: view.session,
    path: view.path,
    path_noq: view.path_noq,
    host: view.host,
    referrer: view.referrer,
    referrer_domain: view.referrer_domain,
    ua: view.ua,
    ua_name: view.ua_name,
    ua_version: view.ua_version,
    os_name: view.os_name,
    os_version: view.os_version,
    device_type: view.device_type,
    screen_w: view.screen_w,
    screen_h: view.screen_h,
    tz_offset: view.tz_offset,
    loc_city: view.loc_city,
    loc_country: view.loc_country,
    loc_lat: view.loc_lat,
    loc_lon: view.loc_lon,
    is_bot: view.is_bot,
  };
}
