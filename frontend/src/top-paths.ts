import { CURRENT_THRESHOLD, PATH_LIST_MAX_LEN } from "./config.js";
import { EventStore } from "./event-store.js";
import { IPageView } from "./page-view.js";
import { Table } from "./table.js";

export class TopPaths extends Table {
  private sessions: Map<string, IPageView>;

  constructor(el: HTMLElement, eventStore: EventStore) {
    super(el, eventStore, ["#", "Path"], false);

    this.sessions = new Map();
  }

  public add(view: IPageView) {
    this.sessions.set(view.session, view);
    this.render();
  }

  public expire(view: IPageView) {
    if (this.sessions.get(view.session) === view) {
      this.sessions.delete(view.session);
    }

    this.render();
  }

  public render() {
    const hitMap = [...this.sessions.values()].reduce((acc, view) => {
      let hits = 1;
      if (acc.has(view.path_noq)) {
        hits += acc.get(view.path_noq)!;
      }

      acc.set(view.path_noq, hits);
      return acc;
    }, new Map() as Map<string, number>);

    const hitList = Array.from(hitMap.entries());
    hitList.sort((a, b) => {
      return b[1] - a[1];
    });

    let transposedList = hitList.map(([path, hits]) => ([hits, path]));
    transposedList = transposedList.slice(0, PATH_LIST_MAX_LEN - 1);
    super.setData(transposedList);
    super.render();
  }
}
