import { Component } from "./component.js";
import { EventStore } from "./event-store.js";

type CellData = string | number | HTMLElement;
type RowData = CellData[];

export abstract class Table extends Component {
  private data: RowData[];
  private table: HTMLTableElement;
  private tbody: HTMLTableSectionElement;

  constructor(el: HTMLElement, eventStore: EventStore, headings: string[], showFooter: boolean = true) {
    super(el, eventStore);

    this.data = [];
    this.table = document.createElement("table");

    const thead = document.createElement("thead");
    const tfoot = document.createElement("tfoot");
    for (const heading of headings) {
      const th = document.createElement("th");
      th.innerText = heading;
      thead.append(th);
      if (showFooter) { tfoot.append(th.cloneNode(true)); }
    }

    this.tbody = document.createElement("tbody");

    this.table.append(thead);
    if (showFooter) { this.table.append(tfoot); }
    this.table.append(this.tbody);
    this.el.append(this.table);
  }

  public setData(data: RowData[]) {
    this.data = data;
  }

  public render() {
    while (this.tbody.firstChild) {
      this.tbody.removeChild(this.tbody.firstChild);
    }

    for (const row of this.data) {
      const tr = document.createElement("tr");

      for (const cell of row) {
        const td = document.createElement("td");

        if (cell instanceof HTMLElement) {
          td.append(cell);
        } else {
          td.innerText = String(cell);
        }

        tr.append(td);
      }

      this.tbody.append(tr);
    }
  }
}
