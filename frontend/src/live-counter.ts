import { Component } from "./component.js";
import { CURRENT_THRESHOLD, LOCALE } from "./config.js";
import { EventStore } from "./event-store.js";
import { IPageView } from "./page-view.js";

/**
 * LiveCounter shows the amount of users on the site in the last 5 minutes.
 */
export class LiveCounter extends Component {
  private sessions: Map<string, number>;
  private numberFormatter: Intl.NumberFormat;

  constructor(el: HTMLElement, eventStore: EventStore) {
    super(el, eventStore);

    this.sessions = new Map();
    this.numberFormatter = new Intl.NumberFormat(LOCALE);
  }

  public add(view: IPageView) {
    const amount: number = this.sessions.get(view.session) || 0;
    this.sessions.set(view.session, amount + 1);
    this.render();
  }

  public expire(view: IPageView) {
    if (this.sessions.has(view.session)) {
      const amount = this.sessions.get(view.session)!;
      if (amount <= 1) {
        this.sessions.delete(view.session);
      } else {
        this.sessions.set(view.session, amount - 1);
      }
    }

    this.render();
  }

  public render() {
    this.el.innerText = this.numberFormatter.format(this.sessions.size);
  }
}
