# Tilastokeskus

Tilastokeskus (also the name of an unrelated Finnish government agency) is a minimal analytics
tool written in Elixir, using Raxx, and PostgreSQL storage. It is currently in early development,
but I use it for my own sites.

The point of Tilastokeskus is to replace Google Analytics on my own sites, but as I use very few
features from GA, the features in Tilastokeskus are also minimal. The motivation for writing my
own analytics tool was to 1) have 1st party analytics, 2) get rid of GA, 3) learn Raxx.

## Currently implemented features

* Collection of data and storing to PostgreSQL
* Full referrers with and without queries + domains
* Simple session system with cookie to group one session's requests
* Parsing of UA with ua_detector library
* GeoIP using MaxMind databases
* Support for allowing a set of hostnames and forbidding others
* Autoremoval of sensitive data with configurable interval
* Simple frontend
* Realtime view to show users currently on site

## Future features

* UI with different graphs to visualize data
* Take DNT header into account somehow

## Won't be implemented (out of scope)

* User accounts/authentication (use HTTP basic auth or SSH tunneling or something else)
* Integrations to 3rd party systems
* Invasive tracking (Flash cookies, dirty tricks)

## Before compiling

* Get deps with `mix deps.get`
* Download/update UA database with `mix ua_inspector.download`.
* Download Maxmind databases from their website (you need an account). Get the GeoLite2 city and
  country databases in Gzip format. Extract the mmdb files to `priv/maxmind/<city|country>`
  directories.

## Environment variables to set for runtime

* `TILASTOKESKUS_HOSTS`: Comma separated list of hostnames to allow analytics tracking for.
* `TILASTOKESKUS_SCRUB_DAYS`: Scrub information from events that are older than this many days.
  Default: 90.
* `RAXX_SECRET`: Secret to use when generating session cookie for user.
* `PORT`: Port to listen on. Default: 1971.
* `DATABASE_URL`: Ecto format URL for database, like this: `ecto://<user>:<pass>@<host>/<db>`.
  Default (for dev): `ecto://tilastokeskus:tilastokeskus@localhost/tilastokeskus`.
