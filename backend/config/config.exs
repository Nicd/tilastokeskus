import Config

# This configuration is loaded before any dependency and is restricted
# to this project. If another project depends on this project, this
# file won't be loaded nor affect the parent project. For this reason,
# if you want to provide default values for your application for
# 3rd-party users, it should be done in your "mix.exs" file.

# You can configure your application as:
#
#     config :tilastokeskus, key: :value
#
# and access this configuration in your application as:
#
#     Application.get_env(:tilastokeskus, :key)
#
# You can also configure a 3rd-party app:
#
#     config :logger, level: :info
#

# It is also possible to import configuration files, relative to this
# directory. For example, you can emulate configuration per environment
# by uncommenting the line below and defining dev.exs, test.exs and such.
# Configuration from the imported file will override the ones defined
# here (which is why it is important to import them last).
#
#     import_config "#{Mix.env}.exs"

config :tilastokeskus,
  ecto_repos: [Tilastokeskus.Archive.Repo]

config :tilastokeskus, Tilastokeskus.Archive.Repo,
  url: "ecto://tilastokeskus:tilastokeskus@localhost/tilastokeskus",
  types: Tilastokeskus.Archive.PostgresTypes

if Mix.env() != :prod do
  config :ua_inspector,
    # This is for the DB download mix tasks
    database_path: "priv/ua"
else
  config :ua_inspector,
    # This is for runtime configuration
    init: {Tilastokeskus.Reception.UADetector, :init}
end

config :geolix,
  init: {Tilastokeskus.Reception.Geolix, :init}

# Don't print every request in production
if Mix.env() == :prod do
  config :logger,
    level: :error
end
