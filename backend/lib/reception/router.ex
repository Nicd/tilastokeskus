defmodule Tilastokeskus.Reception.Router do
  use Raxx.Router

  section([{Raxx.Logger, level: :debug}], [
    {%{method: :POST, path: ["track"]}, Tilastokeskus.Reception.Routes.PageView},
    {%{method: :GET, path: ["live", _host]}, Tilastokeskus.Reception.Routes.LiveView},
    {_, Tilastokeskus.Reception.Routes.NotFound}
  ])
end
