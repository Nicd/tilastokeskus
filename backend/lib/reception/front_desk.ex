defmodule Tilastokeskus.Reception.FrontDesk do
  @moduledoc """
  Entry point for the Raxx web service.
  """

  # Load files at compile time
  @files Raxx.Static.setup(source: Path.expand("priv/static"))

  def child_spec(server_options) do
    {:ok, port} = Keyword.fetch(server_options, :port)

    %{
      id: {__MODULE__, port},
      start: {__MODULE__, :start_link, [server_options]},
      type: :supervisor
    }
  end

  def init(init_arg) do
    {:ok, init_arg}
  end

  def start_link(server_options) do
    {:ok, hosts} = Keyword.fetch(server_options, :hosts)

    stack =
      Raxx.Stack.new(
        [
          {Raxx.Static, @files}
        ],
        {Tilastokeskus.Reception.Router, hosts}
      )

    Ace.HTTP.Service.start_link(stack, server_options)
  end
end
