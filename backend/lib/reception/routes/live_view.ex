defmodule Tilastokeskus.Reception.Routes.LiveView do
  use Raxx.Server

  alias Tilastokeskus.Archive.Schemas.PageView

  @heartbeat_delay 30_000
  @initial_data_mins 5

  @typep req_state :: nil
  @typep resp :: {[Raxx.Response.t()], req_state}

  @impl Raxx.Server
  @spec handle_head(Raxx.Request.t(), any) :: resp
  def handle_head(%{method: :GET, path: ["live", host]}, _state) do
    resp =
      response(200)
      |> set_header("content-type", "text/event-stream")
      |> set_header("access-control-allow-origin", "*")
      |> set_body(true)

    # Register to listen for live updates
    Tilastokeskus.Archive.Archivist.register(host)

    # Start heartbeat ping
    start_heartbeat_timer()

    # Send initial data ASAP
    Process.send_after(self(), {:init, host}, 0)

    {[resp], nil}
  end

  @spec handle_info(any, req_state) :: resp
  def handle_info(msg, state)

  def handle_info({:archivist_update, %PageView{} = view}, state) do
    {[view |> transform_view() |> data()], state}
  end

  def handle_info(:heartbeat, state) do
    # Hearbeat, why do you miss when my baby kisses me?
    start_heartbeat_timer()
    {[data(":ping\n\n")], state}
  end

  def handle_info({:init, host}, state) do
    # Get last minutes' data immediately
    immediate_data =
      Tilastokeskus.Archive.Utils.PageView.get_last(host, @initial_data_mins)
      |> Enum.map(&transform_view/1)
      |> Enum.join("")

    immediate_data = if immediate_data == "", do: ":nil", else: immediate_data

    {[data(immediate_data)], state}
  end

  def handle_info(_, state), do: {[], state}

  # Heartbeat, why does a love kiss stay in my memory?
  defp start_heartbeat_timer(), do: Process.send_after(self(), :heartbeat, @heartbeat_delay)

  # Transform view to SSE event
  defp transform_view(view) do
    content =
      Jason.encode!(%{
        at: view.at,
        addr: view.addr |> :inet_parse.ntoa() |> to_string(),
        extra: view.extra,
        scrubbed: view.scrubbed,
        session: view.session_id,
        path: view.path,
        path_noq: view.path_noq,
        host: view.host,
        referrer: view.referrer,
        referrer_domain: view.referrer_domain,
        ua: view.ua,
        ua_name: view.ua_name,
        ua_version: view.ua_version,
        os_name: view.os_name,
        os_version: view.os_version,
        device_type: view.device_type,
        screen_w: view.screen_w,
        screen_h: view.screen_h,
        tz_offset: view.tz_offset,
        loc_city: view.loc_city,
        loc_country: view.loc_country,
        loc_lat: view.loc_lat,
        loc_lon: view.loc_lon,
        is_bot: view.is_bot
      })

    content_data_rows = String.split(content, "\n") |> Enum.join("\ndata: ")
    "event: view\ndata: #{content_data_rows}\n\n"
  end
end
