defmodule Tilastokeskus.Reception.Routes.PageView do
  use Raxx.SimpleServer
  alias Tilastokeskus.Archive.Schemas.PageView
  require Logger

  @impl Raxx.SimpleServer
  def handle_request(req, state) do
    body = URI.decode_query(req.body || "")
    {path, path_noq, host, authority} = parse_url(body)

    if not check_host(host, state) do
      response(400)
      |> set_header("content-type", "application/json")
      |> set_body(Jason.encode!(%{error: "Host not allowed."}))
    else
      at = DateTime.utc_now()

      addr = get_addr(req)
      {ua, ua_name, ua_version, os_name, os_version, device_type, is_bot} = parse_ua(req)
      {referrer, referrer_noq, referrer_domain} = parse_referrer(body)

      screen_w = Map.get(body, "screen_width")
      screen_h = Map.get(body, "screen_height")
      tz_offset = Map.get(body, "tz_offset")

      {city, {lat, lon}, country} = get_geoip(addr)

      lat = if not is_nil(lat), do: lat, else: 0.0
      lon = if not is_nil(lon), do: lon, else: 0.0

      # Run in one transaction to avoid multiple DB checkouts
      {:ok, response} =
        Tilastokeskus.Archive.Repo.transaction(fn ->
          session = Tilastokeskus.Reception.Session.extract(req)

          cset =
            PageView.changeset(
              %PageView{},
              %{
                at: at,
                session_id: session.id,
                addr: addr,
                path: path,
                path_noq: path_noq,
                host: authority,
                referrer: referrer,
                referrer_noq: referrer_noq,
                referrer_domain: referrer_domain,
                ua: ua,
                ua_name: ua_name,
                ua_version: ua_version,
                os_name: os_name,
                os_version: os_version,
                device_type: device_type,
                screen_w: screen_w,
                screen_h: screen_h,
                tz_offset: tz_offset,
                loc_city: city,
                loc_country: country,
                loc_lat: lat,
                loc_lon: lon,
                is_bot: is_bot
              }
            )

          case Tilastokeskus.Archive.Utils.PageView.create(cset) do
            {:ok, _} ->
              response(200)
              |> set_header("content-type", "application/json")
              |> set_body(Jason.encode!(%{ok: "OK"}))
              |> Tilastokeskus.Reception.Session.embed(session)

            {:error, err} ->
              Logger.error("Error saving pageview: #{inspect(err)}")

              response(500)
              |> set_header("content-type", "application/json")
              |> set_body(Jason.encode!(%{error: "ERROR"}))
          end
        end)

      response
    end
  end

  defp check_host(host, state) do
    allowed_hosts = Keyword.get(state, :hosts, [])

    # If no hosts are specified, everything goes
    Enum.empty?(allowed_hosts) || Enum.member?(allowed_hosts, host)
  end

  defp get_addr(req) do
    case Raxx.get_header(req, "x-forwarded-for") do
      nil ->
        get_ace_ip()

      ip ->
        String.split(ip, ",")
        |> hd()
        |> String.to_charlist()
        |> :inet.parse_address()
        |> case do
          {:ok, ip} -> ip
          _ -> nil
        end
    end
  end

  defp get_ace_ip() do
    with info <- Process.get(Ace.HTTP.Channel),
         {:tcp, socket} <- info.socket,
         {:ok, {ip, _}} <- :inet.peername(socket) do
      ip
    else
      _ -> nil
    end
  end

  defp parse_ua(req) do
    Raxx.get_header(req, "user-agent", nil)
    |> UAInspector.parse()
    |> case do
      %UAInspector.Result{} = ua ->
        {
          ua.user_agent,
          unknown_2_str(ua.client.name),
          unknown_2_str(ua.client.version),
          unknown_2_str(ua.os.name),
          unknown_2_str(ua.os.version),
          unknown_2_str(ua.device.type),
          false
        }

      %UAInspector.Result.Bot{} = ua ->
        {
          ua.user_agent,
          unknown_2_str(ua.name),
          "n/a",
          "n/a",
          "n/a",
          "n/a",
          true
        }
    end
  end

  defp parse_referrer(body) do
    referrer = Map.get(body, "referrer")

    case referrer do
      nil ->
        {nil, nil, nil}

      referrer ->
        referrer_uri = URI.parse(referrer)

        referrer_noq =
          case String.split(referrer, "?") do
            [h | _] -> h
            _ -> referrer
          end

        {
          referrer,
          referrer_noq,
          referrer_uri.authority
        }
    end
  end

  defp parse_url(%{} = body) do
    case Map.get(body, "url") do
      nil ->
        {nil, nil, nil, nil}

      url ->
        parsed = URI.parse(url)

        {
          if(not is_nil(parsed.query), do: "#{parsed.path}?#{parsed.query}", else: parsed.path),
          parsed.path,
          parsed.host,
          parsed.authority
        }
    end
  end

  defp get_geoip(addr) do
    %{city: city, country: country} = Geolix.lookup(addr, as: :raw)

    {
      get_in(city, [:city, :names, :en]),
      {get_in(city, [:location, :latitude]), get_in(city, [:location, :longitude])},
      get_in(country, [:country, :names, :en])
    }
  end

  defp unknown_2_str(:unknown), do: "n/a"
  defp unknown_2_str(val), do: val
end
