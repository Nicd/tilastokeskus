defmodule Tilastokeskus.Reception.Routes.NotFound do
  use Raxx.SimpleServer

  @impl Raxx.SimpleServer
  def handle_request(_req, _state) do
    response(404)
    |> set_header("content-type", "application/json")
    |> set_body(Jason.encode!(%{error: "Route not found."}))
  end
end
