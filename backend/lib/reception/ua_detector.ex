defmodule Tilastokeskus.Reception.UADetector do
  @moduledoc """
  Configuration for `:ua_inspector` and UA utils.
  """

  @doc """
  Configure `:ua_inspector`.
  """
  @spec init() :: :ok
  def init() do
    priv_dir = Application.app_dir(:tilastokeskus, "priv") |> Path.join("ua")
    Application.put_env(:ua_inspector, :database_path, priv_dir)
  end
end
