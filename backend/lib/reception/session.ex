defmodule Tilastokeskus.Reception.Session do
  @moduledoc """
  Raxx session configuration for storing tracking session in client's cookies.
  """

  @doc """
  Validate that the necessary environment variables are setup for creating Raxx config.
  """
  @spec validate_config() :: :ok | no_return
  def validate_config() do
    case System.get_env("RAXX_SECRET") do
      nil ->
        raise "Missing environment variable RAXX_SECRET that is required for signing client cookies."

      _secret ->
        :ok
    end
  end

  @doc """
  Create Raxx SignedCookie config for generating cookies for client.
  """
  @spec config() :: Raxx.Session.t()
  def config() do
    secret = System.get_env("RAXX_SECRET")

    Raxx.Session.config(
      store: Raxx.Session.SignedCookie,
      secret_key_base: secret,
      salt: "statistikcentralen",
      http_only: true,
      key: "tilastokeskus-session-id"
    )
  end

  @doc """
  Embed given session into the response.
  """
  @spec embed(Raxx.Response.t(), map) :: Raxx.Response.t()
  def embed(%Raxx.Response{} = response, session) when is_map(session) do
    Raxx.Session.embed(
      response,
      session,
      config()
    )
  end

  @doc """
  Extract session from request or create new session.
  """
  @spec extract(Raxx.Request.t()) :: map
  def extract(%Raxx.Request{} = request) do
    case Raxx.Session.unprotected_extract(request, config()) do
      {:ok, %{id: id}} ->
        case Tilastokeskus.Archive.Utils.Session.find_by_id(id) do
          nil -> new_session()
          session -> %{id: session.id}
        end

      _ ->
        new_session()
    end
  end

  defp new_session() do
    {:ok, session} = Tilastokeskus.Archive.Utils.Session.create()
    %{id: session.id}
  end
end
