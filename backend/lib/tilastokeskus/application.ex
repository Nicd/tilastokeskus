defmodule Tilastokeskus.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    :ok = Tilastokeskus.Reception.Session.validate_config()

    port = (System.get_env("PORT") || "1971") |> String.to_integer()
    hosts = get_hosts()
    days = get_days()

    # List all child processes to be supervised
    children = [
      # PubSub system for live updates using registry
      Tilastokeskus.Archive.Archivist.child_tuple(),
      {Tilastokeskus.Archive.Repo, []},
      {Tilastokeskus.Archive.Scrubinator, %{days: days}},
      {Tilastokeskus.Reception.FrontDesk, [hosts: hosts, port: port, cleartext: true]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Tilastokeskus.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp get_hosts() do
    case System.get_env("TILASTOKESKUS_HOSTS") do
      nil -> []
      hosts -> String.split(hosts, ",")
    end
  end

  defp get_days() do
    case System.get_env("TILASTOKESKUS_SCRUB_DAYS") do
      nil -> 90
      days -> String.to_integer(days)
    end
  end
end
