defmodule Tilastokeskus.Archive.Schemas.Event do
  @moduledoc """
  An event that can be a pageview, or some other action by the user or backend.
  """

  require Tilastokeskus.Archive.Schemas.EventCommons
  use Ecto.Schema
  import Ecto.Changeset

  schema "events" do
    Tilastokeskus.Archive.Schemas.EventCommons.fields()
  end

  @doc """
  Get a changeset for creating a new event.
  """
  @spec changeset(Ecto.Schema.t(), map) :: Ecto.Changeset.t()
  def changeset(data, params \\ %{}) do
    data
    |> cast(params, [:at, :type, :addr, :extra, :session_id])
    |> validate_required([:at])
  end
end
