defmodule Tilastokeskus.Archive.Schemas.EventCommons do
  @moduledoc """
  Common stuff for both events and pageviews.
  """

  @doc """
  Common fields for both events and pageviews, to avoid duplication.
  """
  defmacro fields() do
    quote do
      Ecto.Schema.field(:at, :utc_datetime)
      Ecto.Schema.field(:type, :string)
      Ecto.Schema.field(:addr, Tilastokeskus.Archive.Types.Inet)

      # Extra information
      Ecto.Schema.field(:extra, :map)

      # Has the row been scrubbed of identifying information?
      Ecto.Schema.field(:scrubbed, :boolean)

      Ecto.Schema.belongs_to(:session, Tilastokeskus.Archive.Schemas.Session, type: Ecto.UUID)
    end
  end
end
