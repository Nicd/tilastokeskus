defmodule Tilastokeskus.Archive.Schemas.PageView do
  @moduledoc """
  A page view of a certain page.
  """

  require Tilastokeskus.Archive.Schemas.EventCommons
  use Ecto.Schema
  import Ecto.Changeset

  schema "events" do
    Tilastokeskus.Archive.Schemas.EventCommons.fields()

    # Full request path including query string
    field(:path, :string)

    # Request path without query string
    field(:path_noq, :string)

    # Request URL host (authority)
    field(:host, :string)

    # Full HTTP referrer
    field(:referrer, :string)
    # Referrer without query string
    field(:referrer_noq, :string)
    # Just the domain of the referrer
    field(:referrer_domain, :string)

    # Full user agent string
    field(:ua, :string)

    field(:ua_name, :string)
    field(:ua_version, :string)
    field(:os_name, :string)
    field(:os_version, :string)
    field(:device_type, :string)

    # Screen size
    field(:screen_w, :integer)
    field(:screen_h, :integer)

    # User's local timezone offset to UTC in minutes
    field(:tz_offset, :integer)

    # GeoIP calculated information
    field(:loc_city, :string)
    field(:loc_country, :string)
    field(:loc_lat, :float)
    field(:loc_lon, :float)

    # Was the hit from a bot
    field(:is_bot, :boolean)
  end

  @doc """
  Get the event type for page views.
  """
  @spec event_type() :: String.t()
  def event_type(), do: "page_view"

  @spec changeset(Ecto.Schema.t(), map) :: Ecto.Changeset.t()
  def changeset(data, params \\ %{}) do
    Tilastokeskus.Archive.Schemas.Event.changeset(data, params)
    |> cast(params, [
      :path,
      :path_noq,
      :host,
      :referrer,
      :referrer_noq,
      :referrer_domain,
      :ua,
      :ua_name,
      :ua_version,
      :os_name,
      :os_version,
      :device_type,
      :screen_w,
      :screen_h,
      :tz_offset,
      :loc_city,
      :loc_country,
      :loc_lat,
      :loc_lon,
      :is_bot
    ])
    |> put_change(:type, event_type())
  end
end
