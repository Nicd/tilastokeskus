defmodule Tilastokeskus.Archive.Schemas.Session do
  @moduledoc """
  A single user session that can be used to track a user's actions on the site.
  """

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type Ecto.UUID

  schema "sessions" do
    field(:opened_at, :utc_datetime)
  end

  @doc """
  Get a changeset for creating a new session.
  """
  @spec changeset(Ecto.Schema.t(), map) :: Ecto.Changeset.t()
  def changeset(data, params \\ %{}) do
    data
    |> cast(params, [:opened_at])
  end

  @doc """
  Maximum valid time of a session in seconds, after this time a new session is created
  """
  @spec max_session_validity() :: integer
  def max_session_validity(), do: 60 * 15
end
