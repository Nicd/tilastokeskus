defmodule Tilastokeskus.Archive.Repo do
  use Ecto.Repo,
    otp_app: :tilastokeskus,
    adapter: Ecto.Adapters.Postgres

  def init(_type, config) do
    config =
      case System.get_env("DATABASE_URL") do
        nil -> config
        url -> Keyword.put(config, :url, url)
      end

    {:ok, config}
  end
end
