defmodule Tilastokeskus.Archive.ReleaseMigrator do
  @moduledoc """
  Tasks for migrating the DB that can be run from a release.
  """

  @app :tilastokeskus

  def migrate() do
    Application.load(@app)

    for repo <- repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def rollback(repo, version) do
    Application.load(@app)
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  defp repos() do
    Application.fetch_env!(@app, :ecto_repos)
  end
end
