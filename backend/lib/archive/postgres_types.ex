Postgrex.Types.define(
  Tilastokeskus.Archive.PostgresTypes,
  [] ++ Ecto.Adapters.Postgres.extensions(),
  json: Jason
)
