defmodule Tilastokeskus.Archive.Utils.Session do
  @moduledoc """
  Session related utilities.
  """

  import Ecto.Query, only: [from: 2]
  alias Tilastokeskus.Archive.Repo
  alias Tilastokeskus.Archive.Schemas.{Session, Event}

  @doc """
  Return a non-expired session by ID, if one exists, otherwise returns nil. Event model is used
  to check for the latest hit of the session to see if it's expired.
  """
  @spec find_by_id(String.t()) :: %Session{} | nil
  def find_by_id(id) do
    res =
      from(
        s in Session,
        left_join: e in Event,
        on: e.session_id == s.id,
        where: s.id == ^id,
        group_by: s.id,
        select: {s, max(e.at)}
      )
      |> Repo.one()

    case res do
      nil ->
        nil

      {session, at} ->
        now = DateTime.utc_now()

        # If session has no hits for some reason, use opened_at date in place of "at"
        at = if not is_nil(at), do: at, else: session.opened_at

        if DateTime.diff(now, at) <= Session.max_session_validity() do
          session
        else
          nil
        end
    end
  end

  @doc """
  Create new session.
  """
  @spec create() :: {:ok, %Session{}} | {:error, term}
  def create() do
    Session.changeset(%Session{}, %{opened_at: DateTime.utc_now() |> DateTime.truncate(:second)})
    |> Repo.insert()
  end
end
