defmodule Tilastokeskus.Archive.Utils.PageView do
  @moduledoc """
  Pageview related utilities.
  """

  import Ecto.Query, only: [from: 2]

  alias Tilastokeskus.Archive.Repo
  alias Tilastokeskus.Archive.Schemas.PageView

  @doc """
  Create new pageview from changeset.
  """
  @spec create(Ecto.Changeset.t()) :: {:ok, PageView.t()} | {:error, term}
  def create(changeset) do
    case Repo.insert(changeset) do
      {:ok, %PageView{} = view} ->
        Tilastokeskus.Archive.Archivist.update(view)
        {:ok, view}

      e ->
        e
    end
  end

  @doc """
  Get page views for the last `mins` minutes for the given host.
  """
  @spec get_last(String.t(), integer) :: [PageView.t()]
  def get_last(host, mins) do
    now = DateTime.utc_now()
    then = DateTime.add(now, -(mins * 60))

    from(pw in PageView,
      where: pw.host == ^host and pw.at >= ^then
    )
    |> Repo.all()
  end
end
