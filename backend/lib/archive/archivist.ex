defmodule Tilastokeskus.Archive.Archivist do
  @moduledoc """
  The archivist is responsible for updating clients about updates to the archive. When new data
  is inserted, the archivist should be notified. It will then notify every client waiting for
  new information.
  """

  alias Tilastokeskus.Archive.Schemas.PageView

  @doc """
  Get the name to use for the registry process.
  """
  @spec registry_name() :: atom
  def registry_name(), do: __MODULE__

  @doc """
  Get name to use for registry key when listening to updates on the given host.
  """
  @spec listen_key(String.t()) :: {atom, String.t()}
  def listen_key(host) when is_binary(host), do: {:archivist_listener, host}

  @doc """
  Return child tuple to add to supervisor to start archivist. If name is not given,
  `registry_name/0` is used.
  """
  @spec child_tuple(atom) :: {module, list}
  def child_tuple(name \\ nil) do
    {Registry,
     [
       name: if(not is_nil(name), do: name, else: registry_name()),
       keys: :duplicate,
       partitions: System.schedulers_online()
     ]}
  end

  @doc """
  Register to listen for updates to a given host.
  """
  @spec register(String.t()) :: {:ok, pid} | {:error, any}
  def register(host) when is_binary(host) do
    Registry.register(registry_name(), listen_key(host), [])
  end

  @doc """
  Send a page view update to any applicable listeners.
  """
  @spec update(PageView.t()) :: :ok
  def update(%PageView{} = view) do
    Registry.dispatch(registry_name(), listen_key(view.host), fn entries ->
      for {pid, _} <- entries, do: send(pid, {:archivist_update, view})
    end)
  end
end
