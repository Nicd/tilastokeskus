defmodule Tilastokeskus.Archive.Scrubinator do
  @moduledoc """
  Scrubinator is a timed assassin that periodically scrubs log data from too sensitive
  information.
  """

  @how_often 24 * 60 * 60 * 1000

  use GenServer
  alias Tilastokeskus.Archive.Schemas.PageView
  alias Tilastokeskus.Archive.Repo
  import Ecto.Query, only: [from: 2]

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  def init(%{days: days} = state) do
    :ok = scrub(days)

    Process.send_after(self(), :scrub, @how_often)
    {:ok, state}
  end

  def handle_info(:scrub, %{days: days} = state) do
    :ok = scrub(days)

    Process.send_after(self(), :scrub, @how_often)
    {:noreply, state}
  end

  @doc """
  Scrub hits older than `days` days of private data.
  """
  @spec scrub(integer) :: :ok
  def scrub(days) do
    now = DateTime.utc_now() |> DateTime.to_unix()
    then = now - days * 24 * 60 * 60

    case DateTime.from_unix(then) do
      {:ok, then_dt} ->
        from(
          p in PageView,
          where: p.at <= ^then_dt and p.scrubbed == false,
          update: [
            set: [
              scrubbed: true,
              addr: nil,
              ua: nil,
              loc_city: nil,
              loc_lat: 0.0,
              loc_lon: 0.0
            ]
          ]
        )
        |> Repo.update_all([])

        :ok

      {:error, _} ->
        :ok
    end
  end
end
