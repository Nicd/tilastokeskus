defmodule Tilastokeskus.MixProject do
  use Mix.Project

  def project do
    [
      app: :tilastokeskus,
      version: "0.3.1",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Tilastokeskus.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:raxx, "~> 1.1.0"},
      {:raxx_static, "~> 0.8.3"},
      {:raxx_logger, "~> 0.2.2"},
      {:raxx_session, "~> 0.2.5"},
      {:ace, "~> 0.18.10"},
      {:postgrex, ">= 0.0.0"},
      {:ecto, "~> 3.4"},
      {:ecto_sql, "~> 3.4"},
      {:jason, "~> 1.2"},
      {:ua_inspector, "~> 2.0"},
      {:geolix, "~> 1.0"},
      {:geolix_adapter_mmdb2, "~> 0.3.0"}
    ]
  end
end
