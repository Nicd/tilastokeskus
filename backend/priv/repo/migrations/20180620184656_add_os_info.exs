defmodule Tilastokeskus.Archive.Repo.Migrations.AddOsInfo do
  use Ecto.Migration

  def change do
    rename(table(:events), :scrubbed_ua, to: :ua_name)
    rename(table(:events), :scrubbed_ua_version, to: :ua_version)

    alter table(:events) do
      add(:os_name, :text, null: true)
      add(:os_version, :text, null: true)
      add(:device_type, :text, null: true)
    end

    create(index(:events, :device_type))
  end
end
