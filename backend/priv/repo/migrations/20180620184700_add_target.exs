defmodule Tilastokeskus.Archive.Repo.Migrations.AddTarget do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add(:path, :text, null: true)
      add(:path_noq, :text, null: true)
      add(:host, :text, null: true)
    end
  end
end
