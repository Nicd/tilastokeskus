defmodule Tilastokeskus.Archive.Repo.Migrations.IsBot do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add(:is_bot, :boolean, default: false)
    end
  end
end
