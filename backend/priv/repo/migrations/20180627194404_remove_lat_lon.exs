defmodule Tilastokeskus.Archive.Repo.Migrations.RemoveLatLon do
  use Ecto.Migration

  def change do
    alter table(:events) do
      remove(:loc_lat)
      remove(:loc_lon)
    end
  end
end
