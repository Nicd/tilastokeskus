defmodule Tilastokeskus.Archive.Repo.Migrations.Initial do
  use Ecto.Migration

  def change do
    create table(:sessions, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:opened_at, :timestamp, null: false)
    end

    create table(:events, primary_key: false) do
      add(:id, :bigserial, primary_key: true)
      add(:at, :timestamp, null: false)

      add(
        :session_id,
        references("sessions", on_delete: :nilify_all, on_update: :update_all, type: :uuid)
      )

      add(:type, :string, null: false, default: "event")
      add(:addr, :inet, null: true)
      add(:extra, :jsonb, null: false, default: "{}")
      add(:scrubbed, :boolean, null: false, default: false)

      add(:referrer, :text, null: true)
      add(:referrer_noq, :text, null: true)
      add(:referrer_domain, :text, null: true)

      add(:ua, :text, null: true)
      add(:scrubbed_ua, :text, null: true)
      add(:scrubbed_ua_version, :text, null: true)
      add(:screen_w, :integer, null: true)
      add(:screen_h, :integer, null: true)
      add(:tz_offset, :integer, null: true)
      add(:loc_lat, :"double precision", null: true)
      add(:loc_lon, :"double precision", null: true)
      add(:loc_city, :text, null: true)
      add(:loc_country, :text, null: true)
    end

    create(index(:events, :session_id))
    create(index(:events, ["at DESC"]))
    create(index(:events, :type))
    create(index(:events, :referrer_domain))
  end
end
