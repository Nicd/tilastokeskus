defmodule Tilastokeskus.Archive.Repo.Migrations.AddLatLon do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add(:loc_lat, :"double precision", null: false, default: 0.0)
      add(:loc_lon, :"double precision", null: false, default: 0.0)
    end
  end
end
